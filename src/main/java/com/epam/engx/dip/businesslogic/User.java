package com.epam.engx.dip.businesslogic;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import com.epam.engx.dip.persistance.models.OrderModel;

public abstract class User {

	private static final double FIVE_PERCENT_DISCOUNT = 0.05;
	private static final double TEN_PERCENT_DISCOUNT = 0.1;
	private static final double FIFTEEN_PERCENT_DISCOUNT = 0.15;
	private static final int DISCOUNT_LEVEL1_THRESHOLD_USD = 1000;
	private static final int DISCOUNT_LEVEL2_THRESHOLD_USD = 3000;
	private static final int DISCOUNT_LEVEL3_THRESHOLD_USD = 5000;
	private Map<Integer, Double> discountThresholdPercentageMap;
	
	public User() {
		discountThresholdPercentageMap = new TreeMap<>(Collections.reverseOrder());
		discountThresholdPercentageMap.put(DISCOUNT_LEVEL1_THRESHOLD_USD, FIVE_PERCENT_DISCOUNT);
		discountThresholdPercentageMap.put(DISCOUNT_LEVEL2_THRESHOLD_USD, TEN_PERCENT_DISCOUNT);
		discountThresholdPercentageMap.put(DISCOUNT_LEVEL3_THRESHOLD_USD, FIFTEEN_PERCENT_DISCOUNT);
	}
	
	public BigDecimal calculateDiscountForUser(User user) {
		double totalPriceOfAllOrders = calculateTotalPriceOfAllOrders(user);
		for (Entry<Integer, Double> entry : discountThresholdPercentageMap.entrySet()) {
			if (totalPriceOfAllOrders >= entry.getKey()) {
				return BigDecimal.valueOf(entry.getValue());
			}
		}
		return BigDecimal.ZERO;
	}
	
	private double calculateTotalPriceOfAllOrders(User user) {
		return user.getOrders()
				.stream()
				.mapToDouble(order -> order.getGrossOrderPrice().doubleValue())
				.sum();
	}
	
	public abstract List<Order> getOrders();

	public abstract void setOrders(List<Order> orderModels);

}
