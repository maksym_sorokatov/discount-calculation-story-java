package com.epam.engx.dip.persistance.models;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.epam.engx.dip.businesslogic.User;

public class UserModel {
	
	private int id;
	private String firstName;
	private String lastName;
	private String email;
	private List<OrderModel> orderModels = new ArrayList<>();
	private Date signUpDate;
	private Date lastLoginDate;
	private int refererId; 
	private String partnerCode;
	private String firebaseCloudMessagingToken;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<OrderModel> getOrderModels() {
		return orderModels;
	}
	public void setOrderModels(List<OrderModel> orderModels) {
		this.orderModels = orderModels;
	}
	public Date getSignUpDate() {
		return signUpDate;
	}
	public void setSignUpDate(Date signUpDate) {
		this.signUpDate = signUpDate;
	}
	public Date getLastLoginDate() {
		return lastLoginDate;
	}
	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}
	public int getRefererId() {
		return refererId;
	}
	public void setRefererId(int refererId) {
		this.refererId = refererId;
	}
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	public String getFirebaseCloudMessagingToken() {
		return firebaseCloudMessagingToken;
	}
	public void setFirebaseCloudMessagingToken(String firebaseCloudMessagingToken) {
		this.firebaseCloudMessagingToken = firebaseCloudMessagingToken;
	}
}
