package com.epam.engx.dip.persistance;

import com.epam.engx.dip.businesslogic.Order;

public class PersistenceOrder extends Order {
	
	private int id;
	private double orderPrice;
	private double totalTax;

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public double getOrderPrice() {
		return this.orderPrice;
	}

	@Override
	public double getTotalTax() {
		return this.totalTax;
	}

	@Override
	public void setOrderPrice(double orderPrice) {
		this.orderPrice = orderPrice;
	}

	@Override
	public void setTotalTax(double totalTax) {
		this.totalTax = totalTax;
	}

}
