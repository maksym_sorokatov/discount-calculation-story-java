package com.epam.engx.dip;

import java.math.BigDecimal;

import com.epam.engx.dip.businesslogic.User;
import com.epam.engx.dip.businesslogic.gateways.UserDao;

public class UserFacade {

	private UserDao userDao;
	
	public BigDecimal getDiscountForUserById(int userId) {
		User user = userDao.getUserById(userId);
		return user.calculateDiscountForUser(user);
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	
	
}
